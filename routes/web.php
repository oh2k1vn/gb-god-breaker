<?php

use App\Http\Controllers\adminController;
use Illuminate\Support\Facades\Route;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;
use SebastianBergmann\Template\Template;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('template.home');
});


Route::get('/login', function() {
    return view('template.login');
});

Route::get('/dashboard', function() {
    return view('admin.home');
});

Route::post('/admin-dashboard', [adminController::class, 'dashboard']);
Route::get('/logout', [adminController::class, 'logout']);