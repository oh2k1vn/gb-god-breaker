<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

session_start();

class adminController extends Controller
{
    public function show_dashboard() {
        return view('admin.home');
    }

    public function dashboard(Request $request)
    {
        $email = $request->email;
        $password = md5($request->password);

        $demo = DB::table('users')->where('email', $email)->where('password', $password)->first();

        if ($demo) {
            session::put('name', $demo->name);
            session::put('id', $demo->id);
            return Redirect::to('/dashboard');
        } else {
            Session::put('message', 'Tài khoản hoặc mật khẩu sai. Bạn hãy thử lại!');
            return Redirect::to('/login');
        }
    }

    public function logout(Request $request)
    {
        session::put('name', null);
        session::put('id', null);
        return Redirect::to('/login');
    }
}
