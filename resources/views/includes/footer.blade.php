<div class="footer">
    <div class="footer_top">
        <div class="footer_top_item">
            <h3>Công ty</h3>
            <ul>
                <li><a href="#">Về GB - Breaker</a></li>
                <li><a href="#">Địa chỉ cửa hàng</a></li>
                <li><a href="#">Đặt đồng phục</a></li>
            </ul>
        </div>
        <div class="footer_top_item">
            <h3>Chính sách</h3>
            <ul>
                <li><a href="#">FAQ</a></li>
                <li><a href="#">Đăng nhập/Đăng ký thành viên</a></li>
                <li><a href="#">Chính sách thành viên</a></li>
                <li><a href="#">Chính sách bảo mật</a></li>
                <li><a href="#">Chính sách bảo hàng</a></li>
                <li><a href="#">Thanh toán & vận chuyển</a></li>
            </ul>
        </div>
        <div class="footer_top_item">
            <h3>Kết nối</h3>
            <ul>
                <li><a href="#">Facebook</a></li>
                <li><a href="#">youtube</a></li>
                <li><a href="#">tiktok</a></li>
                <li><a href="#">Hotline: 1900 232313</a></li>
                <li><a href="#">Email: online@boo.vn</a></li>
            </ul>
        </div>
    </div>
</div>