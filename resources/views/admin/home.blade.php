@extends('admin', ['pageId' => 'homeadmin'])

@section('title', 'Trang chủ admin')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
@endsection

@section('content')
    <h1>Trang chủ admin</h1>


    <a href="{{ URL::to('/logout') }}">Đăng xuất</a>
@endsection